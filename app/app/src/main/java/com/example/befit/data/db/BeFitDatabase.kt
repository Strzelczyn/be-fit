package com.example.befit.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.befit.data.model.*

@Database(
    entities = [Shop::class, Product::class, Calculator::class, CalculatorProduct::class, ShoppingList::class, ShoppingListProduct::class],
    version = 1,
    exportSchema = false
)
abstract class BeFitDatabase : RoomDatabase() {

    abstract fun shopDao(): ShopDao

    abstract fun productDao(): ProductDao

    abstract fun calculatorDao(): CalculatorDao

    abstract fun calculatorProductDao(): CalculatorProductDao

    abstract fun shoppingListDao(): ShoppingListDao

    abstract fun shoppingListProductDao(): ShoppingListProductDao

    companion object {
        @Volatile
        private var INSTANCE: BeFitDatabase? = null
        fun getDatabase(context: Context): BeFitDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BeFitDatabase::class.java,
                    "be_fit_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}