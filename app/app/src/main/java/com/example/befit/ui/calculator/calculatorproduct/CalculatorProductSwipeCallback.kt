package com.example.befit.ui.calculator.calculatorproduct

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R

class CalculatorProductSwipeCallback(
    private val view: View,
    private val adapter: CalculatorProductAdapter,
    private val onClick: (Int) -> Unit
) : ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
    0,
    ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        when (direction) {
            ItemTouchHelper.RIGHT -> {
                onClick.invoke(viewHolder.adapterPosition)
            }
        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        val itemView = viewHolder.itemView
        val drawIcon: Drawable

        val paint = Paint()
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true

        if (dX > 0) {
            paint.color = Color.RED
            c.drawRoundRect(
                RectF(
                    itemView.left.toFloat(),
                    itemView.top.toFloat(),
                    dX.toInt().toFloat(),
                    itemView.bottom.toFloat()
                ), 10F, 10F, paint
            )
            val deleteIcon = view.resources.getDrawable(R.drawable.ic_delete)
            val iconMarginVertical =
                (viewHolder.itemView.height - deleteIcon.intrinsicHeight) / 2
            deleteIcon.setBounds(
                itemView.left + iconMarginVertical,
                itemView.top + iconMarginVertical,
                itemView.left + iconMarginVertical + deleteIcon.intrinsicWidth,
                itemView.bottom - iconMarginVertical
            )
            drawIcon = deleteIcon

            c.save()

            drawIcon.draw(c)

            c.restore()

        }
    }
})