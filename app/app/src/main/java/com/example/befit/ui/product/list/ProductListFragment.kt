package com.example.befit.ui.product.list

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.befit.R
import com.example.befit.data.model.Product
import com.example.befit.databinding.FragmentProductListBinding
import kotlinx.android.synthetic.main.fragment_product_list.view.*

class ProductListFragment : Fragment() {

    private val args by navArgs<ProductListFragmentArgs>()

    private lateinit var productListViewModel: ProductListViewModel

    private lateinit var binding: FragmentProductListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductListBinding.inflate(layoutInflater, container, false)
        productListViewModel = ViewModelProvider(this).get(ProductListViewModel::class.java)
        binding.productlistviewmodel = productListViewModel
        productListViewModel.readAllProductForShop =
            productListViewModel.repository.readAllProductForShop(args.currentShop.id)
        binding.lifecycleOwner = this
        val view = binding.root
        registerObserver()
        ProductListSwipeCallback(
            view,
            productListViewModel.adapter,
            productListViewModel
        ).attachToRecyclerView(view.recyclerViewProductList)
        return view
    }

    private fun registerObserver() {
        productListViewModel.readAllProductForShop.observe(viewLifecycleOwner, Observer {
            productListViewModel.adapter.setData(it)
        })
        productListViewModel.goToProductAdd.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val action =
                    ProductListFragmentDirections.actionProductListFragmentToProductAddFragment(
                        args.currentShop
                    )
                findNavController().navigate(action)
                productListViewModel.goToProductAdd.value = null
            }
        })
        productListViewModel.goToProductUpdate.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val action =
                    ProductListFragmentDirections.actionProductListFragmentToProductUpdateFragment(
                        it
                    )
                findNavController().navigate(action)
                productListViewModel.goToProductUpdate.value = null
            }
        })
        productListViewModel.goToShopList.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                requireActivity().onBackPressed()
                productListViewModel.goToShopList.value = null
            }
        })
        productListViewModel.goToCompare.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                findNavController().navigate(R.id.action_productListFragment_to_compareFragment)
                productListViewModel.goToCompare.value = null
            }
        })
        productListViewModel.goToCalculator.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_productListFragment_to_calculatorFragment)
                productListViewModel.goToCalculator.value = false
            }
        })
        productListViewModel.goToShoppingList.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_productListFragment_to_shoppingListFragment)
                productListViewModel.goToShoppingList.value = false
            }
        })
        productListViewModel.callProductDeleteDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                productDeleteDialog(it)
                productListViewModel.callProductDeleteDialog.value = null
            }
        })
    }

    private fun productDeleteDialog(product: Product) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(resources.getString(R.string.shop_delete_positive)) { _, _ ->
            productListViewModel.productShop(product)
        }
        builder.setNegativeButton(resources.getString(R.string.shop_delete_negative)) { _, _ ->
            productListViewModel.adapter.notifyDataSetChanged()
        }
        builder.setTitle("${resources.getString(R.string.shop_delete_title)} ${product.name}")
        builder.setMessage("${resources.getString(R.string.shop_delete_message)} ${product.name}?")
        builder.create().show()
    }
}
