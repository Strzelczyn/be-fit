package com.example.befit.ui.product.update

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.befit.R
import com.example.befit.databinding.FragmentProductUpdateBinding
import kotlinx.android.synthetic.main.layout_choose_photo_alert.view.*

class ProductUpdateFragment : Fragment() {

    private val args by navArgs<ProductUpdateFragmentArgs>()

    private lateinit var productUpdateViewModel: ProductUpdateViewModel

    private lateinit var binding: FragmentProductUpdateBinding

    companion object {
        const val READ_EXTERNAL_STORAGE_REQUEST_CODE = 1001
        const val TAKE_PHOTO_REQUEST_CODE = 1000
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        productUpdateViewModel = ViewModelProvider(this).get(ProductUpdateViewModel::class.java)
        binding = FragmentProductUpdateBinding.inflate(layoutInflater, container, false)
        productUpdateViewModel.defaultImageViewProductAddImage =
            resources.getDrawable(R.drawable.ic_product)
        if (args.curentProduct.image != null) {
            var imageViewProductUpdateImageBitmap = BitmapFactory.decodeByteArray(
                args.curentProduct.image,
                0,
                args.curentProduct.image!!.size
            )
            productUpdateViewModel.imageViewProductImage.value =
                BitmapDrawable(resources, imageViewProductUpdateImageBitmap)
        } else {
            productUpdateViewModel.imageViewProductImage.value =
                resources.getDrawable(R.drawable.ic_product)
        }
        productUpdateViewModel.productName = args.curentProduct.name
        productUpdateViewModel.energyValue = args.curentProduct.energyValue.toString()
        productUpdateViewModel.fat = args.curentProduct.fat.toString()
        productUpdateViewModel.carbohydrates = args.curentProduct.carbohydrates.toString()
        productUpdateViewModel.fiber = args.curentProduct.fiber.toString()
        productUpdateViewModel.protein = args.curentProduct.protein.toString()
        productUpdateViewModel.salt = args.curentProduct.salt.toString()
        productUpdateViewModel.description = args.curentProduct.description
        productUpdateViewModel.shopID = args.curentProduct.shopId
        productUpdateViewModel.productID = args.curentProduct.id
        binding.productupdateviewmodel = productUpdateViewModel
        binding.lifecycleOwner = this
        val view = binding.root
        registerObserver()
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                READ_EXTERNAL_STORAGE_REQUEST_CODE -> {
                    val imageUri = data?.data
                    val inputStream = requireActivity().contentResolver.openInputStream(imageUri!!)
                    val imageDrawable = Drawable.createFromStream(inputStream, imageUri.toString())
                    productUpdateViewModel.imageViewProductImage.value = imageDrawable
                }
                TAKE_PHOTO_REQUEST_CODE -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    val imageDrawable: Drawable = BitmapDrawable(resources, imageBitmap)
                    productUpdateViewModel.imageViewProductImage.value = imageDrawable
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            READ_EXTERNAL_STORAGE_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImage()
                }
            }
            TAKE_PHOTO_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhoto()
                }
            }
        }
    }

    private fun registerObserver() {
        productUpdateViewModel.goToProductList.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                requireActivity().onBackPressed()
                productUpdateViewModel.goToProductList.value = null
            }
        })
        productUpdateViewModel.callChooseImageDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                chooseImageDialog()
                productUpdateViewModel.callChooseImageDialog.value = null
            }
        })
        productUpdateViewModel.callChooseHealthyProductDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                chooseHealthyProductDialog()
                productUpdateViewModel.callChooseHealthyProductDialog.value = null
            }
        })
        productUpdateViewModel.incorrectDataMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Toast.makeText(
                    context,
                    resources.getString(R.string.product_update_fail_update),
                    Toast.LENGTH_LONG
                ).show()
                productUpdateViewModel.incorrectDataMessage.value = null
            }
        })
    }

    private fun chooseHealthyProductDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(resources.getString(R.string.product_update_positive)) { _, _ ->
            productUpdateViewModel.updateDataToDatabase(true)
        }
        builder.setNegativeButton(resources.getString(R.string.product_update_negative)) { _, _ ->
            productUpdateViewModel.updateDataToDatabase(false)
        }
        builder.setMessage(resources.getString(R.string.product_update_message))
        builder.create().show()
    }

    private fun chooseImageDialog() {
        val alertBuilder = AlertDialog.Builder(requireContext())
        val alertView = View.inflate(requireContext(), R.layout.layout_choose_photo_alert, null)
        alertBuilder.setView(alertView)
        alertBuilder.setTitle(R.string.product_choose_alert_title)
        var alertDialog = alertBuilder.show()
        alertView.imageViewCamera.setOnClickListener {
            takePhoto()
            alertDialog.dismiss()
        }
        alertView.imageViewExternalStorage.setOnClickListener {
            pickImage()
            alertDialog.dismiss()
        }
    }

    private fun takePhoto() {
        if (ActivityCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            this.startActivityForResult(cameraIntent, TAKE_PHOTO_REQUEST_CODE)
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA),
                TAKE_PHOTO_REQUEST_CODE
            )
        }
    }

    private fun pickImage() {
        if (ActivityCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            this.startActivityForResult(
                Intent.createChooser(
                    intent,
                    resources.getString(R.string.shop_add_select_photo_title)
                ), READ_EXTERNAL_STORAGE_REQUEST_CODE
            )
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                READ_EXTERNAL_STORAGE_REQUEST_CODE
            )
        }
    }
}
