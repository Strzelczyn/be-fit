package com.example.befit.data.repository

import androidx.lifecycle.LiveData
import com.example.befit.data.db.ShoppingListProductDao
import com.example.befit.data.model.ShoppingListProduct

class ShoppingListProductRepository(private val shoppingListProductDao: ShoppingListProductDao) {

    val readAllData: LiveData<List<ShoppingListProduct>> = shoppingListProductDao.readAlldata()

    suspend fun addShoppingListProduct(shoppingListProduct: ShoppingListProduct) {
        shoppingListProductDao.addShoppingListProduct(shoppingListProduct)
    }

    suspend fun updateShoppingListProduct(shoppingListProduct: ShoppingListProduct) {
        shoppingListProductDao.updateShoppingListProduct(shoppingListProduct)
    }

    suspend fun deleteShoppingListProduct(shoppingListProduct: ShoppingListProduct) {
        shoppingListProductDao.deleteShoppingListProduct(shoppingListProduct)
    }
}