package com.example.befit.ui.calculator.calculatorproduct

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Calculator
import com.example.befit.data.model.CalculatorProduct
import com.example.befit.data.repository.CalculatorProductRepository
import com.example.befit.data.repository.CalculatorRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CalculatorProductViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var calculator: Calculator

    lateinit var readAllProductForCalculator: LiveData<List<CalculatorProduct>>

    val repository: CalculatorProductRepository =
        CalculatorProductRepository(BeFitDatabase.getDatabase(application).calculatorProductDao())

    val repositoryCalculator: CalculatorRepository =
        CalculatorRepository(BeFitDatabase.getDatabase(application).calculatorDao())

    val callSelectItem: MutableLiveData<Boolean> = MutableLiveData(false)

    fun addItem() {
        callSelectItem.value = true
    }

    fun calculatorProductDelete(item: CalculatorProduct) {
        deleteCalculatorProduct(item)
        editCalculator(item)
    }

    fun editCalculatorProduct(product: CalculatorProduct, value: Int) {
        product.weight = value
        updateCalculatorProduct(product)
    }

    fun editCalculator(product: CalculatorProduct) {
        calculator.energyValue -= product.weight * (product.energyValue / 100)
        updateCalculator(calculator)
    }

    private fun deleteCalculatorProduct(item: CalculatorProduct) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteCalculatorProduct(item)
        }
    }

    private fun updateCalculatorProduct(item: CalculatorProduct) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateCalculatorProduct(item)
        }
    }

    private fun updateCalculator(item: Calculator) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryCalculator.updateCalculator(item)
        }
    }
}