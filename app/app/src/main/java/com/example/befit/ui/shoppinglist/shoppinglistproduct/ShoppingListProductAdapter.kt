package com.example.befit.ui.shoppinglist.shoppinglistproduct

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R
import com.example.befit.data.model.ShoppingListProduct
import kotlinx.android.synthetic.main.layout_shopping_list_item.view.*

class ShoppingListProductAdapter :
    RecyclerView.Adapter<ShoppingListProductAdapter.ShoppingListProductViewHolder>() {

    private var shoppingListProduct = emptyList<ShoppingListProduct>()

    class ShoppingListProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingListProductViewHolder {
        return ShoppingListProductViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_shopping_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return shoppingListProduct.size
    }

    override fun onBindViewHolder(holder: ShoppingListProductViewHolder, position: Int) {
        val currentItem = shoppingListProduct[position]
        holder.itemView.shoppingListName.text = currentItem.id.toString()
    }

    fun setData(shoppingListProduct: List<ShoppingListProduct>) {
        this.shoppingListProduct = shoppingListProduct
        notifyDataSetChanged()
    }

    fun getItem(position: Int): ShoppingListProduct {
        return shoppingListProduct[position]
    }
}