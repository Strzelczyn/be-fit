package com.example.befit.data.repository

import androidx.lifecycle.LiveData
import com.example.befit.data.db.CalculatorDao
import com.example.befit.data.model.Calculator

class CalculatorRepository(private val calculatorDao: CalculatorDao) {

    val readAllData: LiveData<List<Calculator>> = calculatorDao.readAlldata()

    suspend fun addCalculator(calculator: Calculator) {
        calculatorDao.addCalculator(calculator)
    }

    suspend fun deleteCalculator(calculator: Calculator) {
        calculatorDao.deleteCalculator(calculator)
    }

    suspend fun updateCalculator(calculator: Calculator) {
        calculatorDao.updateCalculator(calculator)
    }
}