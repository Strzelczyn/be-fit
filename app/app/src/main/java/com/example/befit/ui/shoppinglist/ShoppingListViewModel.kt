package com.example.befit.ui.shoppinglist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.ShoppingList
import com.example.befit.data.repository.ShoppingListRepository
import kotlinx.coroutines.launch

class ShoppingListViewModel(application: Application) : AndroidViewModel(application) {

    val goToCalculator: MutableLiveData<Boolean> = MutableLiveData()
    val goToShopList: MutableLiveData<Boolean> = MutableLiveData()
    val goToShoppingList: MutableLiveData<Boolean> = MutableLiveData()
    val goToCompare: MutableLiveData<Boolean> = MutableLiveData()

    private val repository: ShoppingListRepository =
        ShoppingListRepository(BeFitDatabase.getDatabase(application).shoppingListDao())

    val readAllData: LiveData<List<ShoppingList>> = repository.readAllData

    val callAddShoppingList: MutableLiveData<Boolean> = MutableLiveData(false)


    fun onClikGoToListShop() {
        goToShopList.value = true
    }

    fun onClikGoToListShopping() {
        goToShoppingList.value = true
    }

    fun onClikGoToCompare() {
        goToCompare.value = true
    }

    fun onClikGoToCalculator() {
        goToCalculator.value = true
    }

    fun onClikAddShoppingList() {
        callAddShoppingList.value = true
    }

    fun addShopping(text: String) {
        val item = ShoppingList(0, text)
        addShoppingList(item)
    }

    fun deleteShopping(shoppingList: ShoppingList) {
        deleteShoppingList(shoppingList)
    }

    private fun deleteShoppingList(item: ShoppingList) {
        viewModelScope.launch {
            repository.deleteShoppingList(item)
        }
    }

    private fun addShoppingList(item: ShoppingList) {
        viewModelScope.launch {
            repository.addShoppingList(item)
        }
    }

}