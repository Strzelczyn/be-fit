package com.example.befit.ui.shoppinglist.shoppinglistproduct

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.befit.databinding.FragmentShoppingListProductBinding

class ShoppingListProductFragment : Fragment() {

    private lateinit var viewModel: ShoppingListProductViewModel
    private lateinit var binding: FragmentShoppingListProductBinding

    private val adapter = ShoppingListProductAdapter()

    private val args by navArgs<ShoppingListProductFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShoppingListProductBinding.inflate(layoutInflater, container, false)
        viewModel = ViewModelProvider(this).get(ShoppingListProductViewModel::class.java)
        binding.lifecycleOwner = this
        binding.shoppinglistproductviewmodel = viewModel
        binding.recyclerView.adapter = adapter
        registerObserver()
        return binding.root
    }

    private fun registerObserver() {
        viewModel.readAllData.observe(viewLifecycleOwner, Observer {
            adapter.setData(it)
        })
    }
}