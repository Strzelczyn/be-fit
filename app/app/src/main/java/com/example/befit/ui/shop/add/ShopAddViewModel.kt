package com.example.befit.ui.shop.add

import android.app.Application
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Shop
import com.example.befit.data.repository.ShopRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream


class ShopAddViewModel(application: Application) : AndroidViewModel(application) {

    var shopName: String

    var imageViewShopAddImage: MutableLiveData<Drawable>

    lateinit var defaultImageViewShopAddImage: Drawable

    val goToShopList: MutableLiveData<Boolean>

    val callChooseImageDialog: MutableLiveData<Boolean>

    val incorrectDataMessage: MutableLiveData<Boolean>

    private val repository: ShopRepository

    companion object {
        @JvmStatic
        @BindingAdapter("android:image")
        fun loadImage(view: ImageView, imageViewShopAddImage: Drawable) {
            view.setImageDrawable(imageViewShopAddImage)
        }
    }

    init {
        val shopDao = BeFitDatabase.getDatabase(
            application
        ).shopDao()
        repository =
            ShopRepository(shopDao)
        goToShopList = MutableLiveData()
        shopName = ""
        callChooseImageDialog = MutableLiveData()
        imageViewShopAddImage = MutableLiveData()
        incorrectDataMessage = MutableLiveData()
    }

    fun chooseImage() {
        callChooseImageDialog.value = true
    }

    fun insertDataToDatabase() {
        if (inputCheck()) {
            var imageViewShopAddImageBitmapCompressed: ByteArrayOutputStream? = null
            if (imageViewShopAddImage.value!!.constantState != defaultImageViewShopAddImage.constantState) {
                val imageViewShopAddImageBitmap =
                    (imageViewShopAddImage.value as BitmapDrawable).bitmap
                imageViewShopAddImageBitmapCompressed = ByteArrayOutputStream()
                imageViewShopAddImageBitmap.compress(
                    Bitmap.CompressFormat.JPEG,
                    60,
                    imageViewShopAddImageBitmapCompressed
                )
            }
            val shop = Shop(0, shopName, imageViewShopAddImageBitmapCompressed?.toByteArray())
            addShop(shop)
            goToShopList.value = true
        }
    }

    private fun inputCheck(): Boolean {
        if (TextUtils.isEmpty(shopName)) {
            incorrectDataMessage.value = true
            return false
        }
        return true
    }

    private fun addShop(shop: Shop) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addShop(shop)
        }
    }
}