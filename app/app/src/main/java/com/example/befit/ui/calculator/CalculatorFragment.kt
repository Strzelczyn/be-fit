package com.example.befit.ui.calculator

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.befit.R
import com.example.befit.data.model.Calculator
import com.example.befit.databinding.FragmentCalculatorBinding
import kotlinx.android.synthetic.main.fragment_calculator.view.*

class CalculatorFragment : Fragment() {

    private lateinit var calculatorViewModel: CalculatorViewModel
    private lateinit var binding: FragmentCalculatorBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCalculatorBinding.inflate(layoutInflater, container, false)
        calculatorViewModel = ViewModelProvider(this).get(CalculatorViewModel::class.java)
        binding.calculatorlistviewmodel = calculatorViewModel
        binding.lifecycleOwner = this
        registerObserver()
        CalculatorSwipeCallback(
            binding.root,
            calculatorViewModel.adapter,
            calculatorViewModel
        ).attachToRecyclerView(binding.root.recyclerView)
        return binding.root
    }

    private fun registerObserver() {
        calculatorViewModel.readAllData.observe(viewLifecycleOwner, Observer {
            calculatorViewModel.adapter.setData(it)
        })
        calculatorViewModel.goToShopList.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                findNavController().navigate(R.id.action_calculatorFragment_to_shopListFragment)
                calculatorViewModel.goToShopList.value = false
            }
        })
        calculatorViewModel.goToCalculator.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                calculatorViewModel.goToCalculator.value = false
            }
        })
        calculatorViewModel.goToShoppingList.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                findNavController().navigate(R.id.action_calculatorFragment_to_shoppingListFragment)
                calculatorViewModel.goToShoppingList.value = false
            }
        })
        calculatorViewModel.goToCompare.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                findNavController().navigate(R.id.action_calculatorFragment_to_compareFragment)
                calculatorViewModel.goToCompare.value = false
            }
        })
        calculatorViewModel.callCalculatorDeleteDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                productDeleteDialog(it)
                calculatorViewModel.callCalculatorDeleteDialog.value = null
            }
        })
    }

    private fun productDeleteDialog(calculator: Calculator) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(resources.getString(R.string.calculator_delete_positive)) { _, _ ->
            calculatorViewModel.deleteCalculator(calculator)
            calculatorViewModel.deleteCalculatorProducts(calculator.id)
        }
        builder.setNegativeButton(resources.getString(R.string.calculator_delete_negative)) { _, _ ->
            calculatorViewModel.adapter.notifyDataSetChanged()
        }
        builder.setTitle("${resources.getString(R.string.calculator_delete_title)} ${calculator.day}")
        builder.setMessage("${resources.getString(R.string.calculator_delete_message)} ${calculator.day}?")
        builder.create().show()
    }
}