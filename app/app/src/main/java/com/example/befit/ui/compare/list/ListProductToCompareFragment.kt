package com.example.befit.ui.compare.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.befit.databinding.FragmentListProductToCompareBinding

class ListProductToCompareFragment : Fragment() {

    private val args by navArgs<ListProductToCompareFragmentArgs>()

    private lateinit var listProductToCompareViewModel: ListProductToCompareViewModel

    private lateinit var binding: FragmentListProductToCompareBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListProductToCompareBinding.inflate(layoutInflater, container, false)
        listProductToCompareViewModel = ViewModelProvider(this).get(ListProductToCompareViewModel::class.java)
        listProductToCompareViewModel.adapter.key = args.key
        binding.listproducttocompareviewmodel = listProductToCompareViewModel
        binding.lifecycleOwner = this
        registerObserver()
        return binding.root
    }

    private fun registerObserver() {
        listProductToCompareViewModel.readAllData.observe(viewLifecycleOwner, Observer {
            listProductToCompareViewModel.adapter.setData(it)
        })
    }
}