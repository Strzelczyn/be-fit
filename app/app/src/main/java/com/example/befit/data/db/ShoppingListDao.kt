package com.example.befit.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.befit.data.model.ShoppingList

@Dao
interface ShoppingListDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addShoppingList(shoppingList: ShoppingList)

    @Update
    suspend fun updateShoppingList(shoppingList: ShoppingList)

    @Delete
    suspend fun deleteShoppingList(shoppingList: ShoppingList)

    @Query("SELECT * FROM shopping_list_table")
    fun readAlldata(): LiveData<List<ShoppingList>>
}