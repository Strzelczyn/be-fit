package com.example.befit.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.befit.data.model.Shop

@Dao
interface ShopDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addShop(shop: Shop)

    @Update
    suspend fun updateShop(shop: Shop)

    @Delete
    suspend fun deleteShop(shop: Shop)

    @Query("SELECT * FROM shop_table")
    fun readAlldata(): LiveData<List<Shop>>

}