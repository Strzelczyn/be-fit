package com.example.befit.ui.compare.product

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.befit.R
import com.example.befit.data.model.Product
import com.example.befit.databinding.FragmentCompareBinding
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_compare.*


class CompareFragment : Fragment() {

    companion object {
        const val KEY_1 = "product1"
        const val KEY_2 = "product2"
        const val COMPARE_PREFS = "COMPARE_PREFS"
    }

    private lateinit var compareViewModel: CompareViewModel

    private lateinit var binding: FragmentCompareBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCompareBinding.inflate(layoutInflater, container, false)
        compareViewModel = ViewModelProvider(this).get(CompareViewModel::class.java)
        binding.compareviewmodel = compareViewModel
        binding.lifecycleOwner = this
        readPreferences()
        registerObserver()
        registerListener()
        return binding.root
    }

    private fun registerObserver() {
        compareViewModel.goToShopList.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_compareFragment_to_shopListFragment)
                compareViewModel.goToShopList.value = false
            }
        })
        compareViewModel.goToCalculator.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_compareFragment_to_calculatorFragment)
                compareViewModel.goToCalculator.value = false
            }
        })
        compareViewModel.goToListProductToCompare.observe(viewLifecycleOwner, Observer {
            if (it != "") {
                val action =
                    CompareFragmentDirections.actionCompareFragmentToListProductToCompareFragment(
                        it
                    )
                findNavController().navigate(action)
                compareViewModel.goToListProductToCompare.value = ""
            }
        })
        compareViewModel.goToShoppingList.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_compareFragment_to_shoppingListFragment)
                compareViewModel.goToShoppingList.value = false
            }
        })
        compareViewModel.product1Name.observe(viewLifecycleOwner, Observer { checkDifference() })
        compareViewModel.product2Name.observe(viewLifecycleOwner, Observer { checkDifference() })
    }

    private fun registerListener() {
        var product1DataBack =
            findNavController().currentBackStackEntry?.savedStateHandle?.contains(KEY_1)
        if (product1DataBack == true) {
            var result =
                findNavController().currentBackStackEntry?.savedStateHandle?.get<Product>(KEY_1)
            compareViewModel.addProductCompare(
                result, KEY_1
            )
            savePreferences(result, KEY_1)
        }
        var product2DataBack =
            findNavController().currentBackStackEntry?.savedStateHandle?.contains(KEY_2)
        if (product2DataBack == true) {
            var result =
                findNavController().currentBackStackEntry?.savedStateHandle?.get<Product>(KEY_2)
            compareViewModel.addProductCompare(
                result, KEY_2
            )
            savePreferences(result, KEY_2)
        }
    }

    private fun readPreferences() {
        compareViewModel.addProductCompare(
            getPreferencesByKey(KEY_1), KEY_1
        )
        compareViewModel.addProductCompare(
            getPreferencesByKey(KEY_2), KEY_2
        )
    }

    private fun getPreferencesByKey(key: String): Product? {
        val prefs: SharedPreferences? = context?.getSharedPreferences(COMPARE_PREFS, MODE_PRIVATE)
        val gson = Gson()
        val json: String? = prefs?.getString(key, "")
        val obj: Product? = gson.fromJson(json, Product::class.java)
        return obj
    }

    private fun savePreferences(result: Product?, key: String) {
        val prefs: SharedPreferences? =
            context?.getSharedPreferences(COMPARE_PREFS, MODE_PRIVATE)
        val prefsEditor = prefs?.edit()
        val gson = Gson()
        val json: String = gson.toJson(result)
        prefsEditor?.putString(key, json)
        prefsEditor?.commit()
    }

    private fun checkDifference() {
        compareText(
            compareViewModel.product1Name.value,
            compareViewModel.product2Name.value,
            textViewProduct1Name,
            textViewProduct2Name
        )
        compareText(
            compareViewModel.product1EnergyValue.value,
            compareViewModel.product2EnergyValue.value,
            textViewProduct1EnergyValue,
            textViewProduct2EnergyValue
        )
        compareText(
            compareViewModel.product1Fat.value,
            compareViewModel.product2Fat.value,
            textViewProduct1Fat,
            textViewProduct2Fat
        )
        compareText(
            compareViewModel.product1Carbohydrates.value,
            compareViewModel.product2Carbohydrates.value,
            textViewProduct1Carbohydrates,
            textViewProduct2Carbohydrates
        )
        compareText(
            compareViewModel.product1Fiber.value,
            compareViewModel.product2Fiber.value,
            textViewProduct1Fiber,
            textViewProduct2Fiber
        )
        compareText(
            compareViewModel.product1Protein.value,
            compareViewModel.product2Protein.value,
            textViewProduct1Protein,
            textViewProduct2Protein
        )
        compareText(
            compareViewModel.product1Salt.value,
            compareViewModel.product2Salt.value,
            textViewProduct1Salt,
            textViewProduct2Salt
        )
        compareText(
            compareViewModel.product1Description.value,
            compareViewModel.product2Description.value,
            textViewProduct1Description,
            textViewProduct2Description
        )
    }

    private fun compareText(
        value: String?,
        value1: String?,
        itemLeft: TextView?,
        itemRight: TextView?
    ) {
        if (value != value1 && !value.isNullOrEmpty() && !value1.isNullOrEmpty()) {
            setRedTextColor(itemLeft, itemRight)
        } else {
            setBlackTextColor(itemLeft, itemRight)
        }
    }

    private fun setRedTextColor(itemLeft: TextView?, itemRight: TextView?) {
        itemLeft?.setTextColor(resources.getColor(android.R.color.holo_red_dark))
        itemRight?.setTextColor(resources.getColor(android.R.color.holo_red_dark))
    }

    private fun setBlackTextColor(itemLeft: TextView?, itemRight: TextView?) {
        itemLeft?.setTextColor(resources.getColor(android.R.color.black))
        itemRight?.setTextColor(resources.getColor(android.R.color.black))
    }

}