package com.example.befit.data.repository

import androidx.lifecycle.LiveData
import com.example.befit.data.db.CalculatorProductDao
import com.example.befit.data.model.CalculatorProduct

class CalculatorProductRepository(private val calculatorProductDao: CalculatorProductDao) {

    fun readAllProductForCalculator(dayID: Int): LiveData<List<CalculatorProduct>> {
        return calculatorProductDao.readAllProductForCalculator(dayID)
    }

    suspend fun addCalculatorProduct(calculatorProduct: CalculatorProduct) {
        calculatorProductDao.addCalculatorProduct(calculatorProduct)
    }

    suspend fun updateCalculatorProduct(calculatorProduct: CalculatorProduct) {
        calculatorProductDao.updateCalculatorProduct(calculatorProduct)
    }

    suspend fun deleteCalculatorProduct(calculatorProduct: CalculatorProduct) {
        calculatorProductDao.deleteCalculatorProduct(calculatorProduct)
    }

    suspend fun deleteCalculatorProductsByID(dayID: Int) {
        calculatorProductDao.deleteCalculatorProductsByID(dayID)
    }
}