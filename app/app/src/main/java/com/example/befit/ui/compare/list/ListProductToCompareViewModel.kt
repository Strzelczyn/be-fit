package com.example.befit.ui.compare.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Product
import com.example.befit.data.repository.ProductRepository

class ListProductToCompareViewModel(application: Application) : AndroidViewModel(application) {

    val adapter: ListProductToCompareAdapter

    val repository: ProductRepository

    val readAllData: LiveData<List<Product>>

    init {
        val productDao = BeFitDatabase.getDatabase(application).productDao()
        repository = ProductRepository(productDao)
        adapter = ListProductToCompareAdapter()
        readAllData = repository.readAllData
    }
}