package com.example.befit.ui.calculator.calculatorproduct.add

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Calculator
import com.example.befit.data.model.CalculatorProduct
import com.example.befit.data.model.Product
import com.example.befit.data.repository.CalculatorProductRepository
import com.example.befit.data.repository.CalculatorRepository
import com.example.befit.data.repository.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CalculatorProductAddViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var calculator: Calculator

    val repository = ProductRepository(BeFitDatabase.getDatabase(application).productDao())

    val repositoryCalculatorProduct =
        CalculatorProductRepository(BeFitDatabase.getDatabase(application).calculatorProductDao())

    val repositoryCalculator: CalculatorRepository =
        CalculatorRepository(BeFitDatabase.getDatabase(application).calculatorDao())

    val readAllData = repository.readAllData

    fun addcalculatorProduct(product: Product, value: Int) {
        val item = CalculatorProduct(0, calculator.id, product.name, product.energyValue, value)
        addProduct(item)
        editCalculator(item)
    }

    private fun editCalculator(product: CalculatorProduct) {
        calculator.energyValue += product.weight * (product.energyValue / 100)
        updateCalculator(calculator)
    }

    private fun addProduct(product: CalculatorProduct) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryCalculatorProduct.addCalculatorProduct(product)
        }
    }

    private fun updateCalculator(item: Calculator) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryCalculator.updateCalculator(item)
        }
    }
}