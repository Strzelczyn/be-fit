package com.example.befit.ui.shop.list

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R

class ShopListSwipeCallback(
    view: View,
    adapter: ShopListAdapter,
    shopViewModel: ShopListViewModel
) : ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
    0,
    ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
) {

    private lateinit var view: View

    private lateinit var shopListAdapter: ShopListAdapter

    private lateinit var shopListViewModel: ShopListViewModel

    init {
        this.view = view
        this.shopListAdapter = adapter
        this.shopListViewModel = shopViewModel
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        when (direction) {
            ItemTouchHelper.LEFT -> {
                shopListViewModel.swipeShopUpdate(adapter.getItem(viewHolder.adapterPosition))
            }
            ItemTouchHelper.RIGHT -> {
                shopListViewModel.swipeShopDeleteDialog(adapter.getItem(viewHolder.adapterPosition))
            }
        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        val itemView = viewHolder.itemView
        val drawIcon: Drawable

        val paint = Paint()
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true

        if (dX > 0) {
            paint.color = Color.RED
            c.drawRoundRect(
                RectF(
                    itemView.left.toFloat(),
                    itemView.top.toFloat(),
                    dX.toInt().toFloat(),
                    itemView.bottom.toFloat()
                ), 10F, 10F, paint
            )
            val deleteIcon = view.resources.getDrawable(R.drawable.ic_delete)
            val iconMarginVertical =
                (viewHolder.itemView.height - deleteIcon.intrinsicHeight) / 2
            deleteIcon.setBounds(
                itemView.left + iconMarginVertical,
                itemView.top + iconMarginVertical,
                itemView.left + iconMarginVertical + deleteIcon.intrinsicWidth,
                itemView.bottom - iconMarginVertical
            )
            drawIcon = deleteIcon

        } else {
            paint.color = Color.BLUE
            c.drawRoundRect(
                RectF(
                    itemView.right.toFloat() + dX.toInt().toFloat(),
                    itemView.top.toFloat(),
                    itemView.right.toFloat(),
                    itemView.bottom.toFloat()
                ), 10F, 10F, paint
            )
            val editIcon = view.resources.getDrawable(R.drawable.ic_edit)
            val iconMarginVertical =
                (viewHolder.itemView.height - editIcon.intrinsicHeight) / 2
            editIcon.setBounds(
                itemView.right - iconMarginVertical - editIcon.intrinsicWidth,
                itemView.top + iconMarginVertical,
                itemView.right - iconMarginVertical,
                itemView.bottom - iconMarginVertical
            )
            drawIcon = editIcon
        }

        c.save()

        drawIcon.draw(c)

        c.restore()
    }
})