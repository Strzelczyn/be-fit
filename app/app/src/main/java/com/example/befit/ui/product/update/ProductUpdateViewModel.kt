package com.example.befit.ui.product.update

import android.app.Application
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Product
import com.example.befit.data.repository.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

class ProductUpdateViewModel(application: Application) : AndroidViewModel(application) {

    var productName: String

    var imageViewProductImage: MutableLiveData<Drawable>

    var energyValue: String

    var fat: String

    var carbohydrates: String

    var fiber: String

    var protein: String

    var salt: String

    var description: String

    lateinit var defaultImageViewProductAddImage: Drawable

    var productID: Int = 0

    var shopID: Int = 0

    val goToProductList: MutableLiveData<Boolean>

    val callChooseImageDialog: MutableLiveData<Boolean>

    val callChooseHealthyProductDialog: MutableLiveData<Boolean>

    val incorrectDataMessage: MutableLiveData<Boolean>

    private val repository: ProductRepository

    companion object {
        @JvmStatic
        @BindingAdapter("android:image")
        fun loadImage(view: ImageView, imageViewShopAddImage: Drawable) {
            view.setImageDrawable(imageViewShopAddImage)
        }
    }

    init {
        val productDao = BeFitDatabase.getDatabase(
            application
        ).productDao()
        repository =
            ProductRepository(productDao)
        productName = ""
        imageViewProductImage = MutableLiveData()
        energyValue = ""
        fat = ""
        carbohydrates = ""
        fiber = ""
        protein = ""
        salt = ""
        description = ""
        goToProductList = MutableLiveData()
        callChooseImageDialog = MutableLiveData()
        callChooseHealthyProductDialog = MutableLiveData()
        incorrectDataMessage = MutableLiveData()
    }

    fun chooseImage() {
        callChooseImageDialog.value = true
    }

    fun chooseHealthyProduct() {
        callChooseHealthyProductDialog.value = true
    }

    fun updateDataToDatabase(healthyProduct: Boolean) {
        if (inputCheck()) {
            var imageViewShopAddImageBitmapCompressed: ByteArrayOutputStream? = null
            if (imageViewProductImage.value!!.constantState != defaultImageViewProductAddImage.constantState) {
                val imageViewShopAddImageBitmap =
                    (imageViewProductImage.value as BitmapDrawable).bitmap
                imageViewShopAddImageBitmapCompressed = ByteArrayOutputStream()
                imageViewShopAddImageBitmap.compress(
                    Bitmap.CompressFormat.JPEG,
                    60,
                    imageViewShopAddImageBitmapCompressed
                )
            }
            val product = Product(
                productID,
                shopID,
                productName,
                imageViewShopAddImageBitmapCompressed?.toByteArray(),
                energyValue.toFloat(),
                fat.toFloat(),
                carbohydrates.toFloat(),
                fiber.toFloat(),
                protein.toFloat(),
                salt.toFloat(),
                healthyProduct,
                description
            )
            updateProduct(product)
            goToProductList.value = true
        }
    }

    private fun inputCheck(): Boolean {
        if (TextUtils.isEmpty(productName)) {
            incorrectDataMessage.value = true
            return false
        }
        energyValue = checkEmptyNumbers(energyValue)
        fat = checkEmptyNumbers(fat)
        carbohydrates = checkEmptyNumbers(carbohydrates)
        fiber = checkEmptyNumbers(fiber)
        protein = checkEmptyNumbers(protein)
        salt = checkEmptyNumbers(salt)
        try {
            energyValue.toFloat()
            fat.toFloat()
            carbohydrates.toFloat()
            fiber.toFloat()
            protein.toFloat()
            salt.toFloat()
        } catch (exception: NumberFormatException) {
            incorrectDataMessage.value = true
            return false
        }
        return true
    }

    private fun checkEmptyNumbers(value: String): String {
        if (TextUtils.isEmpty(value)) {
            return "0"
        }
        return value
    }

    private fun updateProduct(product: Product) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateProduct(product)
        }
    }
}
