package com.example.befit.ui.calculator

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R
import com.example.befit.data.model.Calculator
import kotlinx.android.synthetic.main.layout_calculator_item.view.*

class CalculatorAdapter : RecyclerView.Adapter<CalculatorAdapter.CalculatorViewHolder>() {

    private var calulatorList = emptyList<Calculator>()

    class CalculatorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalculatorViewHolder {
        return CalculatorViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_calculator_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CalculatorViewHolder, position: Int) {
        val currentItem = calulatorList[position]
        holder.itemView.calculatorListDate.text = currentItem.day
        holder.itemView.calculatorEnergyValue.text = currentItem.energyValue.toString()
        holder.itemView.calculatorListItemLayout.setOnClickListener {
            val action =
                CalculatorFragmentDirections.actionCalculatorFragmentToCalculatorProductFragment(
                    currentItem, currentItem.day
                )
            holder.itemView.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return calulatorList.size
    }

    fun setData(calculator: List<Calculator>) {
        this.calulatorList = calculator
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Calculator {
        return calulatorList[position]
    }
}