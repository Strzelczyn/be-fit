package com.example.befit.ui.calculator.calculatorproduct.add

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R
import com.example.befit.data.model.Product
import kotlinx.android.synthetic.main.layout_product_list_item.view.*

class CalculatorProductAddAdapter(val onClik:(Product) -> Unit) : RecyclerView.Adapter<CalculatorProductAddAdapter.CalculatorProductAddViewHolder>() {

    private var productList = emptyList<Product>()

    class CalculatorProductAddViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        const val LOW_ENERGY_VALUE = 100.0f
        const val AVERAGE_ENERGY_VALUE = 500.0f
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalculatorProductAddViewHolder {
        return CalculatorProductAddViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_product_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: CalculatorProductAddViewHolder, position: Int) {
        val currentItem = productList[position]
        if (currentItem.image != null) {
            holder.itemView.productListImage.setImageBitmap(
                BitmapFactory.decodeByteArray(
                    currentItem.image,
                    0,
                    currentItem.image.size
                )
            )
        }
        holder.itemView.productListName.text = currentItem.name
        if (currentItem.healthyProduct == false) {
            holder.itemView.productListHealthyProductImageView.visibility = View.INVISIBLE
        }
        holder.itemView.productListItemLayout.setOnClickListener {
            onClik.invoke(currentItem)
        }
    }

    fun setData(product: List<Product>) {
        this.productList = product
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Product {
        return productList[position]
    }
}