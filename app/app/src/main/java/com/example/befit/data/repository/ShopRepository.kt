package com.example.befit.data.repository

import androidx.lifecycle.LiveData
import com.example.befit.data.db.ShopDao
import com.example.befit.data.model.Shop

class ShopRepository(private val shopDao: ShopDao) {

    val readAllData: LiveData<List<Shop>> = shopDao.readAlldata()

    suspend fun addShop(shop: Shop) {
        shopDao.addShop(shop)
    }

    suspend fun updateShop(shop: Shop){
        shopDao.updateShop(shop)
    }

    suspend fun deleteShop(shop: Shop){
        shopDao.deleteShop(shop)
    }
}