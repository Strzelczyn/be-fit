package com.example.befit.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "product_table")
data class Product(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val shopId: Int,
    val name: String,
    val image: ByteArray?,
    val energyValue: Float,
    val fat: Float,
    val carbohydrates: Float,
    val fiber: Float,
    val protein: Float,
    val salt: Float,
    val healthyProduct: Boolean,
    val description: String
) : Parcelable
