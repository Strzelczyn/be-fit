package com.example.befit.ui.product.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Product
import com.example.befit.data.repository.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductListViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var readAllProductForShop: LiveData<List<Product>>

    val adapter: ProductListAdapter

    val goToProductAdd: MutableLiveData<Boolean>
    val goToShopList: MutableLiveData<Boolean>
    val goToCompare: MutableLiveData<Boolean>
    val goToShoppingList: MutableLiveData<Boolean>
    val goToCalculator: MutableLiveData<Boolean>

    val goToProductUpdate: MutableLiveData<Product>
    val callProductDeleteDialog: MutableLiveData<Product>

    val repository: ProductRepository

    init {
        val productDao = BeFitDatabase.getDatabase(application).productDao()
        repository = ProductRepository(productDao)
        adapter = ProductListAdapter()
        goToProductAdd = MutableLiveData()
        goToProductUpdate = MutableLiveData()
        goToShopList = MutableLiveData()
        callProductDeleteDialog = MutableLiveData()
        goToCompare = MutableLiveData()
        goToCalculator = MutableLiveData()
        goToShoppingList = MutableLiveData()
    }

    fun onClikGoToProductShop() {
        goToProductAdd.value = true
    }

    fun onClikGoToListShop() {
        goToShopList.value = true
    }

    fun onClikGoToListShopping() {
        goToShoppingList.value = true
    }

    fun onClikGoToCompare() {
        goToCompare.value = true
    }

    fun onClikGoToCalculator() {
        goToCalculator.value = true
    }

    fun productShop(product: Product) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteProduct(product)
        }
    }

    fun swipeProductDeleteDialog(item: Product) {
        callProductDeleteDialog.value = item
    }

    fun swipeProductUpdate(item: Product) {
        goToProductUpdate.value = item
    }
}
