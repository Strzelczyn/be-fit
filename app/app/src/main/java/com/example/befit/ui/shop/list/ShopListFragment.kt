package com.example.befit.ui.shop.list

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.befit.R
import com.example.befit.data.model.Shop
import com.example.befit.databinding.FragmentShopListBinding
import kotlinx.android.synthetic.main.fragment_shop_list.*
import kotlinx.android.synthetic.main.fragment_shop_list.view.*


class ShopListFragment : Fragment() {

    private lateinit var shopViewModel: ShopListViewModel

    private lateinit var binding: FragmentShopListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShopListBinding.inflate(layoutInflater, container, false)
        shopViewModel = ViewModelProvider(this).get(ShopListViewModel::class.java)
        binding.shoplistviewmodel = shopViewModel
        binding.lifecycleOwner = this
        val view = binding.root
        registerObserver()
        ShopListSwipeCallback(
            view,
            shopViewModel.adapter,
            shopViewModel
        ).attachToRecyclerView(view.recyclerView)
        return view
    }

    private fun registerObserver() {
        shopViewModel.readAllData.observe(viewLifecycleOwner, Observer {
            shopViewModel.adapter.setData(it)
        })
        shopViewModel.goToShopAdd.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                findNavController().navigate(R.id.action_shopListFragment_to_addShopFragment)
                shopViewModel.goToShopAdd.value = null
            }
        })
        shopViewModel.goToShopUpdate.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val action =
                    ShopListFragmentDirections.actionShopListFragmentToShopUpdateFragment(
                        it
                    )
                findNavController().navigate(action)
                shopViewModel.goToShopUpdate.value = null
            }
        })
        shopViewModel.goToShopList.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                recyclerView.smoothScrollToPosition(0)
                shopViewModel.goToShopList.value = null
            }
        })
        shopViewModel.callShopDeleteDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                shopDeleteDialog(it)
                shopViewModel.callShopDeleteDialog.value = null
            }
        })
        shopViewModel.goToCompare.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_shopListFragment_to_compareFragment)
                shopViewModel.goToCompare.value = false
            }
        })
        shopViewModel.goToCalculator.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_shopListFragment_to_calculatorFragment)
                shopViewModel.goToCalculator.value = false
            }
        })
        shopViewModel.goToShoppingList.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                findNavController().navigate(R.id.action_shopListFragment_to_shoppingListFragment)
                shopViewModel.goToShoppingList.value = false
            }
        })
    }

    private fun shopDeleteDialog(shop: Shop) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(resources.getString(R.string.shop_delete_positive)) { _, _ ->
            shopViewModel.deleteProductForShop(shop.id)
            shopViewModel.deleteShop(shop)
            Toast.makeText(context,resources.getString(R.string.shop_delete_success_delete), Toast.LENGTH_LONG).show()
        }
        builder.setNegativeButton(resources.getString(R.string.shop_delete_negative)) { _, _ ->
            shopViewModel.adapter.notifyDataSetChanged()
        }
        builder.setTitle("${resources.getString(R.string.shop_delete_title)} ${shop.name}")
        builder.setMessage("${resources.getString(R.string.shop_delete_message)} ${shop.name}?")
        builder.create().show()
    }

}
