package com.example.befit.ui.product.add

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.befit.R
import com.example.befit.databinding.FragmentProductAddBinding
import com.example.befit.ui.shop.add.ShopAddFragment
import kotlinx.android.synthetic.main.layout_choose_photo_alert.view.*


class ProductAddFragment : Fragment() {

    private val args by navArgs<ProductAddFragmentArgs>()

    private lateinit var productAddViewModel: ProductAddViewModel

    private lateinit var binding: FragmentProductAddBinding

    companion object {
        const val READ_EXTERNAL_STORAGE_REQUEST_CODE = 1001
        const val TAKE_PHOTO_REQUEST_CODE = 1000
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductAddBinding.inflate(layoutInflater, container, false)
        productAddViewModel = ViewModelProvider(this).get(ProductAddViewModel::class.java)
        productAddViewModel.imageViewProductImage.value =
            resources.getDrawable(R.drawable.ic_product)
        productAddViewModel.defaultImageViewProductAddImage =
            resources.getDrawable(R.drawable.ic_product)
        productAddViewModel.shopID = args.currentShop.id
        binding.productaddviewmodel = productAddViewModel
        binding.lifecycleOwner = this
        val view = binding.root
        registerObserver()
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                READ_EXTERNAL_STORAGE_REQUEST_CODE -> {
                    val imageUri = data?.data
                    val inputStream = requireActivity().contentResolver.openInputStream(imageUri!!)
                    val imageDrawable = Drawable.createFromStream(inputStream, imageUri.toString())
                    productAddViewModel.imageViewProductImage.value = imageDrawable
                }
                TAKE_PHOTO_REQUEST_CODE -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    val imageDrawable: Drawable = BitmapDrawable(resources, imageBitmap)
                    productAddViewModel.imageViewProductImage.value = imageDrawable
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            ShopAddFragment.READ_EXTERNAL_STORAGE_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImage()
                }
            }
            ShopAddFragment.TAKE_PHOTO_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhoto()
                }
            }
        }
    }

    private fun registerObserver() {
        productAddViewModel.goToProductList.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                requireActivity().onBackPressed()
                productAddViewModel.goToProductList.value = null
            }
        })
        productAddViewModel.callChooseImageDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                chooseImageDialog()
                productAddViewModel.callChooseImageDialog.value = null
            }
        })
        productAddViewModel.callChooseHealthyProductDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                chooseHealthyProductDialog()
                productAddViewModel.callChooseHealthyProductDialog.value = null
            }
        })
        productAddViewModel.incorrectDataMessage.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Toast.makeText(context,resources.getString(R.string.product_add_fail_add), Toast.LENGTH_LONG).show()
                productAddViewModel.incorrectDataMessage.value = null
            }
        })
    }

    private fun chooseHealthyProductDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton(resources.getString(R.string.product_add_positive)) { _, _ ->
            productAddViewModel.insertDataToDatabase(true)
        }
        builder.setNegativeButton(resources.getString(R.string.product_add_negative)) { _, _ ->
            productAddViewModel.insertDataToDatabase(false)
        }
        builder.setMessage(resources.getString(R.string.product_add_message))
        builder.create().show()
    }

    private fun chooseImageDialog() {
        val alertBuilder = AlertDialog.Builder(requireContext())
        val alertView = View.inflate(requireContext(), R.layout.layout_choose_photo_alert, null)
        alertBuilder.setView(alertView)
        alertBuilder.setTitle(R.string.product_choose_alert_title)
        var alertDialog = alertBuilder.show()
        alertView.imageViewCamera.setOnClickListener {
            takePhoto()
            alertDialog.dismiss()
        }
        alertView.imageViewExternalStorage.setOnClickListener {
            pickImage()
            alertDialog.dismiss()
        }
    }

    private fun takePhoto() {
        if (ActivityCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            this.startActivityForResult(cameraIntent, ShopAddFragment.TAKE_PHOTO_REQUEST_CODE)
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA),
                ShopAddFragment.TAKE_PHOTO_REQUEST_CODE
            )
        }
    }

    private fun pickImage() {
        if (ActivityCompat.checkSelfPermission(
                this.requireContext(),
                READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            this.startActivityForResult(
                Intent.createChooser(
                    intent,
                    resources.getString(R.string.shop_add_select_photo_title)
                ), READ_EXTERNAL_STORAGE_REQUEST_CODE
            )
        } else {
            requestPermissions(
                arrayOf(READ_EXTERNAL_STORAGE),
                READ_EXTERNAL_STORAGE_REQUEST_CODE
            )
        }
    }
}
