package com.example.befit.data.repository

import androidx.lifecycle.LiveData
import com.example.befit.data.db.ShoppingListDao
import com.example.befit.data.model.ShoppingList

class ShoppingListRepository(private val shoppingListDao: ShoppingListDao) {

    val readAllData: LiveData<List<ShoppingList>> = shoppingListDao.readAlldata()

    suspend fun addShoppingList(shoppingList: ShoppingList) {
        shoppingListDao.addShoppingList(shoppingList)
    }

    suspend fun updateShoppingList(shoppingList: ShoppingList) {
        shoppingListDao.updateShoppingList(shoppingList)
    }

    suspend fun deleteShoppingList(shoppingList: ShoppingList) {
        shoppingListDao.deleteShoppingList(shoppingList)
    }
}