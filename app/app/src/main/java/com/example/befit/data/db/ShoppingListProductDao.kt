package com.example.befit.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.befit.data.model.ShoppingListProduct

@Dao
interface ShoppingListProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addShoppingListProduct(shoppingListProduct: ShoppingListProduct)

    @Update
    suspend fun updateShoppingListProduct(shoppingListProduct: ShoppingListProduct)

    @Delete
    suspend fun deleteShoppingListProduct(shoppingListProduct: ShoppingListProduct)

    @Query("SELECT * FROM shopping_list_product_table")
    fun readAlldata(): LiveData<List<ShoppingListProduct>>
}