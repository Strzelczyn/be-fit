package com.example.befit.ui.calculator.calculatorproduct.add

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.befit.R
import com.example.befit.data.model.Product
import com.example.befit.databinding.FragmentCalculatorProductAddBinding
import kotlinx.android.synthetic.main.custom_dialog_calculator_product_add.view.*

class CalculatorProductAddFragment : Fragment() {

    private lateinit var calculatorProductAddViewModel: CalculatorProductAddViewModel

    private lateinit var binding: FragmentCalculatorProductAddBinding

    private var adapter = CalculatorProductAddAdapter { it -> calculatorProductAddDialog(it)}

    private var dialog: AlertDialog? = null

    private val args by navArgs<CalculatorProductAddFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCalculatorProductAddBinding.inflate(layoutInflater, container, false)
        calculatorProductAddViewModel =
            ViewModelProvider(this).get(CalculatorProductAddViewModel::class.java)
        binding.calculatorproductaddviewmodel = calculatorProductAddViewModel
        binding.lifecycleOwner = this
        binding.recyclerViewProductList.adapter = adapter
        calculatorProductAddViewModel.calculator = args.calculator
        registerObserver()
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        hideDialog()
    }

    private fun registerObserver() {
        calculatorProductAddViewModel.readAllData.observe(
            viewLifecycleOwner,
            Observer {
                adapter.setData(it)
            })
    }

    private fun calculatorProductAddDialog(product: Product) {
        hideDialog()
        val view = layoutInflater.inflate(R.layout.custom_dialog_calculator_product_add, null)
        view.numberpicker_main_picker.minValue = 0
        view.numberpicker_main_picker.maxValue = 500
        dialog = AlertDialog.Builder(requireContext()).setView(view)
            .setPositiveButton(resources.getString(R.string.alert_dialog_add_positive_add_calculator_product)) { _, _ ->
                calculatorProductAddViewModel.addcalculatorProduct(product, view.numberpicker_main_picker.value)
                val navController = findNavController()
                navController.popBackStack()
            }
            .setNegativeButton(resources.getString(R.string.alert_dialog_add_negative_add_calculator_product)) { _, _ ->
            }
            .setTitle(resources.getString(R.string.alert_dialog_title_add_calculator_product))
            .setMessage(resources.getString(R.string.alert_dialog_message_add_calculator_product))
            .create()
        dialog?.show()
    }


    private fun hideDialog() {
        dialog?.dismiss()
        dialog = null
    }
}