package com.example.befit.ui.shop.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Shop
import com.example.befit.data.repository.ProductRepository
import com.example.befit.data.repository.ShopRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShopListViewModel(application: Application) : AndroidViewModel(application) {

    val adapter: ShopListAdapter

    val goToShopAdd: MutableLiveData<Boolean>
    val goToCalculator: MutableLiveData<Boolean>
    val goToShopList: MutableLiveData<Boolean>
    val goToShoppingList: MutableLiveData<Boolean>
    val goToCompare: MutableLiveData<Boolean>

    val readAllData: LiveData<List<Shop>>
    val goToShopUpdate: MutableLiveData<Shop>
    val callShopDeleteDialog: MutableLiveData<Shop>

    private val repository: ShopRepository

    private val repositoryProduct: ProductRepository

    init {
        val shopDao = BeFitDatabase.getDatabase(application).shopDao()
        repository = ShopRepository(shopDao)
        val productDao = BeFitDatabase.getDatabase(application).productDao()
        repositoryProduct = ProductRepository(productDao)
        readAllData = repository.readAllData
        goToShopAdd = MutableLiveData()
        callShopDeleteDialog = MutableLiveData()
        goToShopUpdate = MutableLiveData()
        goToShopList = MutableLiveData()
        adapter = ShopListAdapter()
        goToCompare = MutableLiveData()
        goToCalculator = MutableLiveData()
        goToShoppingList = MutableLiveData()
    }

    fun onClikGoToAddShop() {
        goToShopAdd.value = true
    }

    fun onClikGoToListShop() {
        goToShopList.value = true
    }

    fun onClikGoToListShopping() {
        goToShoppingList.value = true
    }

    fun onClikGoToCompare() {
        goToCompare.value = true
    }

    fun onClikGoToCalculator() {
        goToCalculator.value = true
    }

    fun deleteShop(shop: Shop) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteShop(shop)
        }
    }

    fun deleteProductForShop(shopID: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryProduct.deleteProductForShop(shopID)
        }
    }

    fun swipeShopDeleteDialog(item: Shop) {
        callShopDeleteDialog.value = item
    }

    fun swipeShopUpdate(item: Shop) {
        goToShopUpdate.value = item
    }

}