package com.example.befit.data.repository

import androidx.lifecycle.LiveData
import com.example.befit.data.db.ProductDao
import com.example.befit.data.model.Product

class ProductRepository(private val productDao: ProductDao) {

    val readAllData: LiveData<List<Product>> = productDao.readAlldata()

    fun readAllProductForShop(shopID: Int): LiveData<List<Product>> {
        return productDao.readAllProductForShop(shopID)
    }

    fun deleteProductForShop(shopID: Int){
        productDao.deleteProductForShop(shopID)
    }

    suspend fun addProduct(product: Product) {
        productDao.addProduct(product)
    }

    suspend fun updateProduct(product: Product) {
        productDao.updateProduct(product)
    }

    suspend fun deleteProduct(product: Product) {
        productDao.deleteProduct(product)
    }
}