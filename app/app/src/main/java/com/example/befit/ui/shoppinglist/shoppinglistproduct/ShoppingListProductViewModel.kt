package com.example.befit.ui.shoppinglist.shoppinglistproduct

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.ShoppingListProduct
import com.example.befit.data.repository.ShoppingListProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShoppingListProductViewModel(application: Application) : AndroidViewModel(application) {

    val repository = ShoppingListProductRepository(
        BeFitDatabase.getDatabase(application).shoppingListProductDao()
    )

    val readAllData: LiveData<List<ShoppingListProduct>> = repository.readAllData

    fun onClikAddShoppingListItem() {
        val item = ShoppingListProduct(0, 0)
        addProduct(item)
    }

    private fun addProduct(shoppingListProduct: ShoppingListProduct) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addShoppingListProduct(shoppingListProduct)
        }
    }
}