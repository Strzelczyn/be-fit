package com.example.befit.ui.compare.product

import android.app.Application
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.befit.R
import com.example.befit.data.model.Product


class CompareViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        const val KEY_1 = "product1"
        const val KEY_2 = "product2"
    }

    val goToShopList: MutableLiveData<Boolean> = MutableLiveData()
    val goToCalculator: MutableLiveData<Boolean> = MutableLiveData()
    val goToShoppingList: MutableLiveData<Boolean> = MutableLiveData()

    val goToListProductToCompare: MutableLiveData<String> = MutableLiveData()

    val resources = application.resources

    var product1Image: MutableLiveData<Drawable?> =
        MutableLiveData(application.getDrawable(R.drawable.ic_healthy))
    var product2Image: MutableLiveData<Drawable?> =
        MutableLiveData(application.getDrawable(R.drawable.ic_healthy))

    var product1Name: MutableLiveData<String> = MutableLiveData("")
    var product2Name: MutableLiveData<String> = MutableLiveData("")
    var product1EnergyValue: MutableLiveData<String> = MutableLiveData("")
    var product2EnergyValue: MutableLiveData<String> = MutableLiveData("")
    var product1Fat: MutableLiveData<String> = MutableLiveData("")
    var product2Fat: MutableLiveData<String> = MutableLiveData("")
    var product1Carbohydrates: MutableLiveData<String> = MutableLiveData("")
    var product2Carbohydrates: MutableLiveData<String> = MutableLiveData("")
    var product1Fiber: MutableLiveData<String> = MutableLiveData("")
    var product2Fiber: MutableLiveData<String> = MutableLiveData("")
    var product1Protein: MutableLiveData<String> = MutableLiveData("")
    var product2Protein: MutableLiveData<String> = MutableLiveData("")
    var product1Salt: MutableLiveData<String> = MutableLiveData("")
    var product2Salt: MutableLiveData<String> = MutableLiveData("")
    var product1Description: MutableLiveData<String> = MutableLiveData("")
    var product2Description: MutableLiveData<String> = MutableLiveData("")

    fun addProduct1() {
        goToListProductToCompare.value = KEY_1
    }

    fun addProduct2() {
        goToListProductToCompare.value = KEY_2
    }

    fun onClikGoToListShop() {
        goToShopList.value = true
    }

    fun onClikGoToListShopping() {
        goToShoppingList.value = true
    }

    fun onClikGoToCalculator() {
        goToCalculator.value = true
    }

    fun addProductCompare(result: Product?, key: String) {
        when (key) {
            KEY_1 -> addProductToCompare(
                result,
                product1Image,
                product1Name,
                product1EnergyValue,
                product1Fat,
                product1Carbohydrates,
                product1Fiber,
                product1Protein,
                product1Salt,
                product1Description
            )
            KEY_2 -> addProductToCompare(
                result,
                product2Image,
                product2Name,
                product2EnergyValue,
                product2Fat,
                product2Carbohydrates,
                product2Fiber,
                product2Protein,
                product2Salt,
                product2Description
            )
        }
    }

    private fun addProductToCompare(
        result: Product?,
        productImage: MutableLiveData<Drawable?>,
        productName: MutableLiveData<String>,
        productEnergyValue: MutableLiveData<String>,
        productFat: MutableLiveData<String>,
        productCarbohydrates: MutableLiveData<String>,
        productFiber: MutableLiveData<String>,
        productProtein: MutableLiveData<String>,
        productSalt: MutableLiveData<String>,
        productDescription: MutableLiveData<String>
    ) {
        if (result != null) {
            var bitmap = result.image?.size?.let {
                BitmapFactory.decodeByteArray(
                    result.image,
                    0,
                    it
                )
            }
            productImage.value = BitmapDrawable(resources,bitmap)
            productName.value = result.name
            productEnergyValue.value = result.energyValue.toString()
            productFat.value = result.fat.toString()
            productCarbohydrates.value = result.carbohydrates.toString()
            productFiber.value = result.fiber.toString()
            productProtein.value = result.protein.toString()
            productSalt.value = result.salt.toString()
            productDescription.value = result.description
        }
    }
}