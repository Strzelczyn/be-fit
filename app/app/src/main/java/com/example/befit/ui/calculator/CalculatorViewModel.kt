package com.example.befit.ui.calculator

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Calculator
import com.example.befit.data.repository.CalculatorProductRepository
import com.example.befit.data.repository.CalculatorRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class CalculatorViewModel(application: Application) : AndroidViewModel(application) {

    val goToCalculator: MutableLiveData<Boolean> = MutableLiveData()
    val goToShopList: MutableLiveData<Boolean> = MutableLiveData()
    val goToShoppingList: MutableLiveData<Boolean> = MutableLiveData()
    val goToCompare: MutableLiveData<Boolean> = MutableLiveData()

    val callCalculatorDeleteDialog: MutableLiveData<Calculator> = MutableLiveData()

    val adapter: CalculatorAdapter = CalculatorAdapter()

    private val repository: CalculatorRepository =
        CalculatorRepository(BeFitDatabase.getDatabase(application).calculatorDao())

    private val repositoryCalculatorProduct: CalculatorProductRepository =
        CalculatorProductRepository(BeFitDatabase.getDatabase(application).calculatorProductDao())

    val readAllData: LiveData<List<Calculator>> = repository.readAllData


    fun onClikGoToListShop() {
        goToShopList.value = true
    }

    fun onClikGoToListShopping() {
        goToShoppingList.value = true
    }

    fun onClikGoToCompare() {
        goToCompare.value = true
    }

    fun onClikGoToCalculator() {
        goToCalculator.value = true
    }

    fun insertDataToDatabase() {
        val date = SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().time)
        val calculator = Calculator(0, date, 0.0f)
        addCalculator(calculator)
    }

    fun deleteCalculatorProducts(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryCalculatorProduct.deleteCalculatorProductsByID(id)
        }
    }

    fun deleteCalculator(calculator: Calculator) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteCalculator(calculator)
        }
    }

    fun swipeCalculatorDeleteDialog(item: Calculator) {
        callCalculatorDeleteDialog.value = item
    }

    private fun addCalculator(calculator: Calculator) {
        viewModelScope.launch(Dispatchers.IO) {
                repository.addCalculator(calculator)
        }
    }
}