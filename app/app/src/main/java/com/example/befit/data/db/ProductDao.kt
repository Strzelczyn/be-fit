package com.example.befit.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.befit.data.model.Product

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addProduct(product: Product)

    @Update
    suspend fun updateProduct(product: Product)

    @Delete
    suspend fun deleteProduct(product: Product)

    @Query("DELETE FROM product_table WHERE shopId=:shopID")
    fun deleteProductForShop(shopID: Int)

    @Query("SELECT * FROM product_table")
    fun readAlldata(): LiveData<List<Product>>

    @Query("SELECT * FROM product_table WHERE shopId = :shopID")
    fun readAllProductForShop(shopID: Int): LiveData<List<Product>>
}