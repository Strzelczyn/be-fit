package com.example.befit.ui.calculator.calculatorproduct

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R
import com.example.befit.data.model.CalculatorProduct
import kotlinx.android.synthetic.main.layout_calculator_item.view.*

class CalculatorProductAdapter(val onClik: (CalculatorProduct) -> Unit) :
    RecyclerView.Adapter<CalculatorProductAdapter.CalculatorProductViewHolder>() {

    private var calulatorProductList = emptyList<CalculatorProduct>()

    class CalculatorProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalculatorProductViewHolder {
        return CalculatorProductViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_calculator_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CalculatorProductViewHolder, position: Int) {
        val currentItem = calulatorProductList[position]
        holder.itemView.calculatorListDate.text = currentItem.name
        holder.itemView.calculatorEnergyValue.text =
            String.format("%.2f", (currentItem.weight * (currentItem.energyValue / 100)))
        holder.itemView.calculatorListItemLayout.setOnClickListener{
            onClik.invoke(currentItem)
        }
    }

    override fun getItemCount(): Int {
        return calulatorProductList.size
    }

    fun setData(calculator: List<CalculatorProduct>) {
        this.calulatorProductList = calculator
        notifyDataSetChanged()
    }

    fun getItem(position: Int): CalculatorProduct {
        return calulatorProductList[position]
    }
}