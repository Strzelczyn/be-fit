package com.example.befit.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.befit.data.model.Calculator

@Dao
interface CalculatorDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addCalculator(calculator: Calculator)

    @Query("SELECT * FROM calculator_table")
    fun readAlldata(): LiveData<List<Calculator>>

    @Delete
    suspend fun deleteCalculator(calculator: Calculator)

    @Update
    suspend fun updateCalculator(calculator: Calculator)
}