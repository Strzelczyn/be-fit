package com.example.befit.ui.shop.list

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R
import com.example.befit.data.model.Shop
import kotlinx.android.synthetic.main.layout_shop_list_item.view.*


class ShopListAdapter : RecyclerView.Adapter<ShopListAdapter.ShopViewHolder>() {

    private var shopList = emptyList<Shop>()

    class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder {
        return ShopViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_shop_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return shopList.size
    }

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        val currentItem = shopList[position]
        if (currentItem.image != null) {
            holder.itemView.shopListImage.setImageBitmap(
                BitmapFactory.decodeByteArray(
                    currentItem.image,
                    0,
                    currentItem.image.size
                )
            )
        }
        holder.itemView.shopListName.text = currentItem.name
        holder.itemView.shopListItemLayout.setOnClickListener {
            val action =
               ShopListFragmentDirections.actionShopListFragmentToProductListFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData(shop: List<Shop>) {
        this.shopList = shop
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Shop {
        return shopList[position]
    }
}