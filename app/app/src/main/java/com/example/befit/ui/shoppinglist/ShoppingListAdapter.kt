package com.example.befit.ui.shoppinglist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R
import com.example.befit.data.model.ShoppingList
import kotlinx.android.synthetic.main.layout_shopping_list_item.view.*

class ShoppingListAdapter(val onClick: (ShoppingList) -> Unit) :
    RecyclerView.Adapter<ShoppingListAdapter.ShoppingListViewHolder>() {

    private var shoppingList = emptyList<ShoppingList>()

    class ShoppingListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingListViewHolder {
        return ShoppingListViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_shopping_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return shoppingList.size
    }

    override fun onBindViewHolder(holder: ShoppingListViewHolder, position: Int) {
        val currentItem = shoppingList[position]
        holder.itemView.shoppingListName.text = currentItem.name
        holder.itemView.setOnClickListener {
            onClick.invoke(currentItem)
        }
    }

    fun setData(shoppingList: List<ShoppingList>) {
        this.shoppingList = shoppingList
        notifyDataSetChanged()
    }

    fun getItem(position: Int): ShoppingList {
        return shoppingList[position]
    }
}