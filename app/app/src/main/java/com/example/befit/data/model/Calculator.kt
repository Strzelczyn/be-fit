package com.example.befit.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "calculator_table",indices = [androidx.room.Index(
    value = ["day"],
    unique = true
)])

data class Calculator(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val day: String,
    var energyValue: Float
) : Parcelable





