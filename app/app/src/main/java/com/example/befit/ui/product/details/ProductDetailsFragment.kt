package com.example.befit.ui.product.details

import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.befit.R
import com.example.befit.databinding.FragmentProductDetailsBinding

class ProductDetailsFragment : Fragment() {

    private val args by navArgs<ProductDetailsFragmentArgs>()

    private lateinit var productDetailsViewModel: ProductDetailsViewModel

    private lateinit var binding: FragmentProductDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        productDetailsViewModel = ViewModelProvider(this).get(ProductDetailsViewModel::class.java)
        binding = FragmentProductDetailsBinding.inflate(layoutInflater, container, false)
        if (args.curentProduct.image != null) {
            var imageViewProductUpdateImageBitmap = BitmapFactory.decodeByteArray(
                args.curentProduct.image,
                0,
                args.curentProduct.image!!.size
            )
            productDetailsViewModel.imageViewProductImage.value =
                BitmapDrawable(resources, imageViewProductUpdateImageBitmap)
        } else {
            productDetailsViewModel.imageViewProductImage.value =
                resources.getDrawable(R.drawable.ic_product)
        }
        productDetailsViewModel.productName = args.curentProduct.name
        productDetailsViewModel.energyValue = args.curentProduct.energyValue.toString()
        productDetailsViewModel.fat = args.curentProduct.fat.toString()
        productDetailsViewModel.carbohydrates = args.curentProduct.carbohydrates.toString()
        productDetailsViewModel.fiber = args.curentProduct.fiber.toString()
        productDetailsViewModel.protein = args.curentProduct.protein.toString()
        productDetailsViewModel.salt = args.curentProduct.salt.toString()
        productDetailsViewModel.description = args.curentProduct.description
        binding.productdetailsviewmodel = productDetailsViewModel
        binding.lifecycleOwner = this
        val view = binding.root
        return view
    }
}
