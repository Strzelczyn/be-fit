package com.example.befit.ui.product.details

import android.app.Application
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class ProductDetailsViewModel(application: Application) : AndroidViewModel(application) {

    var productName: String

    var imageViewProductImage: MutableLiveData<Drawable>

    var energyValue: String

    var fat: String

    var carbohydrates: String

    var fiber: String

    var protein: String

    var salt: String

    var description: String

    companion object {
        @JvmStatic
        @BindingAdapter("android:image")
        fun loadImage(view: ImageView, imageViewShopAddImage: Drawable) {
            view.setImageDrawable(imageViewShopAddImage)
        }
    }

    init {
        productName = ""
        imageViewProductImage = MutableLiveData()
        energyValue = ""
        fat = ""
        carbohydrates = ""
        fiber = ""
        protein = ""
        salt = ""
        description = ""
    }

}
