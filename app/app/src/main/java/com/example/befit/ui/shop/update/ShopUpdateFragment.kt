package com.example.befit.ui.shop.update

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.befit.R
import com.example.befit.databinding.FragmentShopUpdateBinding
import kotlinx.android.synthetic.main.fragment_shop_update.view.*
import kotlinx.android.synthetic.main.layout_choose_photo_alert.view.*


class ShopUpdateFragment : Fragment() {

    private val args by navArgs<ShopUpdateFragmentArgs>()

    private lateinit var shopUpdateViewModel: ShopUpdateViewModel

    private lateinit var binding: FragmentShopUpdateBinding

    companion object {
        const val READ_EXTERNAL_STORAGE_REQUEST_CODE = 1001
        const val TAKE_PHOTO_REQUEST_CODE = 1000
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentShopUpdateBinding.inflate(layoutInflater, container, false)
        shopUpdateViewModel = ViewModelProvider(this).get(ShopUpdateViewModel::class.java)
        shopUpdateViewModel.defaultImageViewShopAddImage = resources.getDrawable(R.drawable.ic_shop)
        shopUpdateViewModel.shopName = args.currentShop.name
        if (args.currentShop.image != null) {
            var imageViewShopUpdateImageBitmap = BitmapFactory.decodeByteArray(
                args.currentShop.image,
                0,
                args.currentShop.image!!.size
            )
            shopUpdateViewModel.imageViewShopUpdateImage.value =
                BitmapDrawable(resources, imageViewShopUpdateImageBitmap)
        } else {
            shopUpdateViewModel.imageViewShopUpdateImage.value =
                resources.getDrawable(R.drawable.ic_shop)
        }
        shopUpdateViewModel.shopID = args.currentShop.id
        binding.shopupdateviewmodel = shopUpdateViewModel
        binding.lifecycleOwner = this
        val view = binding.root
        view.imageViewShopUpdateImage.setOnClickListener {
            takePhoto()
        }
        registerObserver()
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                READ_EXTERNAL_STORAGE_REQUEST_CODE -> {
                    val imageUri = data?.data
                    val inputStream = requireActivity().contentResolver.openInputStream(imageUri!!)
                    val imageDrawable = Drawable.createFromStream(inputStream, imageUri.toString())
                    shopUpdateViewModel.imageViewShopUpdateImage.value = imageDrawable
                }
                TAKE_PHOTO_REQUEST_CODE -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    val imageDrawable: Drawable = BitmapDrawable(resources, imageBitmap)
                    shopUpdateViewModel.imageViewShopUpdateImage.value = imageDrawable
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            READ_EXTERNAL_STORAGE_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImage()
                }
            }
            TAKE_PHOTO_REQUEST_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhoto()
                }
            }
        }
    }

    private fun registerObserver() {
        shopUpdateViewModel.goToShopList.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                requireActivity().onBackPressed()
                shopUpdateViewModel.goToShopList.value = null
                Toast.makeText(context,resources.getString(R.string.shop_update_success_update), Toast.LENGTH_LONG).show()
            }
        })
        shopUpdateViewModel.callChooseImageDialog.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                chooseImageDialog()
                shopUpdateViewModel.callChooseImageDialog.value = null
            }
        })
    }

    private fun chooseImageDialog() {
        val alertBuilder = AlertDialog.Builder(requireContext())
        val alertView = View.inflate(requireContext(), R.layout.layout_choose_photo_alert, null)
        alertBuilder.setView(alertView)
        alertBuilder.setTitle(R.string.shop_choose_alert_title)
        var alertDialog = alertBuilder.show()
        alertView.imageViewCamera.setOnClickListener {
            takePhoto()
            alertDialog.dismiss()
        }
        alertView.imageViewExternalStorage.setOnClickListener {
            pickImage()
            alertDialog.dismiss()
        }
    }

    private fun takePhoto() {
        if (ActivityCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            this.startActivityForResult(cameraIntent, TAKE_PHOTO_REQUEST_CODE)
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA),
                TAKE_PHOTO_REQUEST_CODE
            )
        }
    }

    private fun pickImage() {
        if (ActivityCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            this.startActivityForResult(
                Intent.createChooser(
                    intent,
                    resources.getString(R.string.shop_add_select_photo_title)
                ), READ_EXTERNAL_STORAGE_REQUEST_CODE
            )
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                READ_EXTERNAL_STORAGE_REQUEST_CODE
            )
        }
    }
}
