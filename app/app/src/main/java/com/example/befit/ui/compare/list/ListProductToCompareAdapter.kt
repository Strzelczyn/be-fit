package com.example.befit.ui.compare.list

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.befit.R
import com.example.befit.data.model.Product
import kotlinx.android.synthetic.main.layout_product_list_item.view.*

class ListProductToCompareAdapter :
    RecyclerView.Adapter<ListProductToCompareAdapter.ProductViewHolder>() {

    lateinit var key: String

    private var productList = emptyList<Product>()

    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_product_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val currentItem = productList[position]
        if (currentItem.image != null) {
            holder.itemView.productListImage.setImageBitmap(
                BitmapFactory.decodeByteArray(
                    currentItem.image,
                    0,
                    currentItem.image.size
                )
            )
        }
        holder.itemView.productListName.text = currentItem.name
        if (currentItem.healthyProduct == false) {
            holder.itemView.productListHealthyProductImageView.visibility = View.INVISIBLE
        }
        holder.itemView.productListItemLayout.setOnClickListener {
            val navController = Navigation.findNavController(holder.itemView)
            navController.previousBackStackEntry?.savedStateHandle?.set(key, currentItem)
            navController.popBackStack()
        }
    }

    fun setData(product: List<Product>) {
        this.productList = product
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Product {
        return productList[position]
    }
}