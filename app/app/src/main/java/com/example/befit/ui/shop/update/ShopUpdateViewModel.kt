package com.example.befit.ui.shop.update

import android.app.Application
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.befit.data.db.BeFitDatabase
import com.example.befit.data.model.Shop
import com.example.befit.data.repository.ShopRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

class ShopUpdateViewModel(application: Application) : AndroidViewModel(application) {

    var shopName: String

    var imageViewShopUpdateImage: MutableLiveData<Drawable>

    var shopID: Int = 0

    lateinit var defaultImageViewShopAddImage: Drawable

    val goToShopList: MutableLiveData<Boolean>

    val callChooseImageDialog: MutableLiveData<Boolean>

    private val repository: ShopRepository

    companion object {
        @JvmStatic
        @BindingAdapter("android:image")
        fun loadImage(view: ImageView, imageViewShopUpdateImage: Drawable) {
            view.setImageDrawable(imageViewShopUpdateImage)
        }
    }

    init {
        val shopDao = BeFitDatabase.getDatabase(application).shopDao()
        repository = ShopRepository(shopDao)
        goToShopList = MutableLiveData()
        shopName = ""
        callChooseImageDialog = MutableLiveData()
        imageViewShopUpdateImage = MutableLiveData()
    }

    fun chooseImage() {
        callChooseImageDialog.value = true
    }

    fun updateDataToDatabase() {
        if (imputcheck(shopName)) {
            if (imageViewShopUpdateImage.value!!.constantState != defaultImageViewShopAddImage.constantState) {
                val imageViewShopUpdateImageBitmap =
                    (imageViewShopUpdateImage.value as BitmapDrawable).bitmap
                val imageViewShopUpdateImageBitmapCompressed = ByteArrayOutputStream()
                imageViewShopUpdateImageBitmap.compress(
                    Bitmap.CompressFormat.JPEG,
                    60,
                    imageViewShopUpdateImageBitmapCompressed
                )
                val shop = Shop(
                    shopID,
                    shopName,
                    imageViewShopUpdateImageBitmapCompressed.toByteArray()
                )
                updateShop(shop)
            } else {
                val shop = Shop(
                    shopID,
                    shopName,
                    null
                )
                updateShop(shop)
            }
            goToShopList.value = true
        }
    }

    private fun imputcheck(name: String): Boolean {
        return !(TextUtils.isEmpty(name))
    }

    private fun updateShop(shop: Shop) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateShop(shop)
        }
    }
}