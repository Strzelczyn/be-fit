package com.example.befit.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.befit.data.model.CalculatorProduct

@Dao
interface CalculatorProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addCalculatorProduct(calculatorProduct: CalculatorProduct)

    @Update
    suspend fun updateCalculatorProduct(calculatorProduct: CalculatorProduct)

    @Delete
    suspend fun deleteCalculatorProduct(calculatorProduct: CalculatorProduct)

    @Query("DELETE FROM calculator_product_table WHERE dayId = :dayID")
    fun deleteCalculatorProductsByID(dayID: Int)

    @Query("SELECT * FROM calculator_product_table WHERE dayId = :dayID")
    fun readAllProductForCalculator(dayID: Int): LiveData<List<CalculatorProduct>>

}