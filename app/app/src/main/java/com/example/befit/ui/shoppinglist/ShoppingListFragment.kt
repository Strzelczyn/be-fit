package com.example.befit.ui.shoppinglist

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.befit.R
import com.example.befit.data.model.ShoppingList
import com.example.befit.databinding.FragmentShoppingListBinding
import kotlinx.android.synthetic.main.fragment_calculator.view.*
import kotlinx.android.synthetic.main.layout_shopping_list_item.view.*

class ShoppingListFragment : Fragment() {

    private lateinit var shoppingListViewModel: ShoppingListViewModel
    private lateinit var binding: FragmentShoppingListBinding

    private var adapter: ShoppingListAdapter = ShoppingListAdapter{ it ->
        val action = ShoppingListFragmentDirections.actionShoppingListFragmentToShoppingListProductFragment(it)
        findNavController().navigate(action)
    }

    private var dialog: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShoppingListBinding.inflate(layoutInflater, container, false)
        shoppingListViewModel = ViewModelProvider(this).get(ShoppingListViewModel::class.java)
        binding.shoppinglistviewmodel = shoppingListViewModel
        binding.lifecycleOwner = this
        binding.recyclerView.adapter = adapter
        ShoppingListSwipeCallback(
            binding.root
        ){it -> deleteDialog(adapter.getItem(it))}.attachToRecyclerView(binding.root.recyclerView)
        registerObserver()
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        hideDialog()
    }

    private fun registerObserver() {
        shoppingListViewModel.readAllData.observe(viewLifecycleOwner, Observer {
            adapter.setData(it)
        })
        shoppingListViewModel.goToShopList.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                findNavController().navigate(R.id.action_shoppingListFragment_to_shopListFragment)
                shoppingListViewModel.goToShopList.value = false
            }
        })
        shoppingListViewModel.goToCalculator.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                findNavController().navigate(R.id.action_shoppingListFragment_to_calculatorFragment)
                shoppingListViewModel.goToCalculator.value = false
            }
        })
        shoppingListViewModel.goToShoppingList.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                shoppingListViewModel.goToShoppingList.value = false
            }
        })
        shoppingListViewModel.goToCompare.observe(viewLifecycleOwner, Observer {
            if (it != false) {
                findNavController().navigate(R.id.action_shoppingListFragment_to_compareFragment)
                shoppingListViewModel.goToCompare.value = false
            }
        })
        shoppingListViewModel.callAddShoppingList.observe(viewLifecycleOwner, Observer {
            if (it) {
                addDialog()
                shoppingListViewModel.callAddShoppingList.value = false
            }
        })
    }

    private fun addDialog() {
        hideDialog()
        val view = layoutInflater.inflate(R.layout.custom_dialog_shopping_list_add, null)
        dialog = AlertDialog.Builder(requireContext()).setView(view)
            .setPositiveButton(resources.getString(R.string.shopping_list_add_positive)) { _, _ ->
                shoppingListViewModel.addShopping(view.shoppingListName.text.toString())
            }
            .setNegativeButton(resources.getString(R.string.shopping_list_add_negative)) { _, _ ->
            }
            .setTitle(resources.getString(R.string.shopping_list_add_title))
            .setMessage(resources.getString(R.string.shopping_list_add_message))
            .create()
        dialog?.show()
    }

    private fun deleteDialog(shoppingList: ShoppingList) {
        hideDialog()
        dialog = AlertDialog.Builder(requireContext())
            .setPositiveButton(resources.getString(R.string.shopping_list_delete_positive)) { _, _ ->
                shoppingListViewModel.deleteShopping(shoppingList)
            }
            .setNegativeButton(resources.getString(R.string.shopping_list_delete_negative)) { _, _ ->
                adapter.notifyDataSetChanged()
            }
            .setTitle("${resources.getString(R.string.shopping_list_delete_title)} ${shoppingList.name}")
            .setMessage("${resources.getString(R.string.shopping_list_delete_message)} ${shoppingList.name}?")
            .create()
        dialog?.show()
    }

    private fun hideDialog() {
        dialog?.dismiss()
        dialog = null
    }
}