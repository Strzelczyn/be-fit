package com.example.befit.ui.calculator.calculatorproduct

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.befit.R
import com.example.befit.data.model.CalculatorProduct
import com.example.befit.databinding.FragmentCalculatorProductBinding
import kotlinx.android.synthetic.main.custom_dialog_calculator_product_add.view.*
import kotlinx.android.synthetic.main.fragment_calculator.view.*
import java.text.SimpleDateFormat
import java.util.*

class CalculatorProductFragment : Fragment() {

    private lateinit var calculatorProductViewModel: CalculatorProductViewModel
    private lateinit var binding: FragmentCalculatorProductBinding

    private var dialog: AlertDialog? = null

    private val adapter: CalculatorProductAdapter =
        CalculatorProductAdapter { it ->   if(calculatorProductViewModel.calculator.day == SimpleDateFormat("dd.MM.yyyy").format(
                Calendar.getInstance().time) ){calculatorProductEditDialog(it)} }

    private val args by navArgs<CalculatorProductFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCalculatorProductBinding.inflate(layoutInflater, container, false)
        calculatorProductViewModel =
            ViewModelProvider(this).get(CalculatorProductViewModel::class.java)
        calculatorProductViewModel.readAllProductForCalculator =
            calculatorProductViewModel.repository.readAllProductForCalculator(args.calculator.id)
        calculatorProductViewModel.calculator = args.calculator
        binding.calculatorproductviewmodel = calculatorProductViewModel
        binding.lifecycleOwner = this
        binding.recyclerView.adapter = adapter
        if(calculatorProductViewModel.calculator.day != SimpleDateFormat("dd.MM.yyyy").format(
                Calendar.getInstance().time) ){
            binding.floatingActionButton2.hide()
        }else{
            CalculatorProductSwipeCallback(
                binding.root,
                adapter
            ) { item ->
                productDeleteDialog(adapter.getItem(item))
            }.attachToRecyclerView(binding.root.recyclerView)
        }
        registerObserver()
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        hideDialog()
    }

    private fun registerObserver() {
        calculatorProductViewModel.readAllProductForCalculator.observe(
            viewLifecycleOwner,
            Observer {
                adapter.setData(it)
            })
        calculatorProductViewModel.callSelectItem.observe(viewLifecycleOwner, Observer {
            if (it) {
                val action = CalculatorProductFragmentDirections.actionCalculatorProductFragmentToCalculatorProductAddFragment(args.calculator)
                findNavController().navigate(action)
                calculatorProductViewModel.callSelectItem.value = false
            }
        })
    }

    private fun productDeleteDialog(item: CalculatorProduct) {
        hideDialog()
        dialog = AlertDialog.Builder(requireContext())
            .setPositiveButton(resources.getString(R.string.calculator_delete_positive)) { _, _ ->
                calculatorProductViewModel.calculatorProductDelete(item)
            }
            .setNegativeButton(resources.getString(R.string.calculator_delete_negative)) { _, _ ->
                adapter.notifyDataSetChanged()
            }
            .setTitle("${resources.getString(R.string.calculator_delete_title)} ${item.dayId}")
            .setMessage("${resources.getString(R.string.calculator_delete_message)} ${item.dayId}?")
            .create()
        dialog?.show()
    }

    private fun calculatorProductEditDialog(product: CalculatorProduct) {
        hideDialog()
        val view = layoutInflater.inflate(R.layout.custom_dialog_calculator_product_add, null)
        view.numberpicker_main_picker.minValue = 0
        view.numberpicker_main_picker.maxValue = 500
        view.numberpicker_main_picker.value = product.weight
        dialog = AlertDialog.Builder(requireContext()).setView(view)
            .setPositiveButton(resources.getString(R.string.alert_dialog_add_positive_edit_calculator_product)) { _, _ ->
                calculatorProductViewModel.editCalculatorProduct(
                    product,
                    view.numberpicker_main_picker.value
                )
            }
            .setNegativeButton(resources.getString(R.string.alert_dialog_add_negative_edit_calculator_product)) { _, _ ->
            }
            .setTitle(resources.getString(R.string.alert_dialog_title_edit_calculator_product))
            .setMessage(resources.getString(R.string.alert_dialog_message_edit_calculator_product))
            .create()
        dialog?.show()
    }

    private fun hideDialog() {
        dialog?.dismiss()
        dialog = null
    }

}